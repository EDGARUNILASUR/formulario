-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-10-2019 a las 04:29:32
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nominas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_empleados`
--

CREATE TABLE `datos_empleados` (
  `num_nomina` int(8) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `apel_pat` varchar(12) NOT NULL,
  `apel_mat` varchar(12) DEFAULT NULL,
  `calle` varchar(30) NOT NULL,
  `num_ex` varchar(10) NOT NULL,
  `num_int` varchar(10) DEFAULT NULL,
  `colonia` varchar(20) NOT NULL,
  `delegacion` varchar(20) NOT NULL,
  `cp` int(5) DEFAULT NULL,
  `pais` varchar(15) DEFAULT NULL,
  `area` varchar(20) DEFAULT NULL,
  `rfc` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `datos_empleados`
--

INSERT INTO `datos_empleados` (`num_nomina`, `nombre`, `apel_pat`, `apel_mat`, `calle`, `num_ex`, `num_int`, `colonia`, `delegacion`, `cp`, `pais`, `area`, `rfc`) VALUES
(1, 'Edgar', 'Salas', 'Lemoine', 'Prolongacion las flores', '297', 'D4', 'Los Reyes', 'Coyoacan', 4330, 'Mexico', 'Gerencia', 'SALE941110'),
(2, 'lalo', 'lemoine', 'jaja', 'kolindancia', '123', '2', 'molcajete', 'lasruinas', 4330, 'usa', 'lalovillar', 'lplplpl'),
(3, 'lalo', 'lemoine', 'jaja', 'kolindancia', '123', '2', 'molcajete', 'lasruinas', 4330, 'usa', 'lalovillar', 'asdasd123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina`
--

CREATE TABLE `nomina` (
  `num_folio` int(8) NOT NULL,
  `num_nomina` int(8) DEFAULT NULL,
  `sal_diario` decimal(8,2) NOT NULL,
  `num_faltas` int(2) NOT NULL,
  `dias_traba` int(2) DEFAULT NULL,
  `val_despensa` decimal(8,2) DEFAULT NULL,
  `ayu_comedor` decimal(8,2) DEFAULT NULL,
  `ayu_transporte` decimal(8,2) DEFAULT NULL,
  `imss` decimal(8,2) DEFAULT NULL,
  `isr` decimal(8,2) DEFAULT NULL,
  `saldo_fin` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datos_empleados`
--
ALTER TABLE `datos_empleados`
  ADD PRIMARY KEY (`num_nomina`),
  ADD UNIQUE KEY `rfc` (`rfc`);

--
-- Indices de la tabla `nomina`
--
ALTER TABLE `nomina`
  ADD PRIMARY KEY (`num_folio`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
